package com.evme.myproject.controller;

import com.evme.myproject.config.MyConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class MyController {
    @Autowired
    private MyConfig myConfig;

    @RequestMapping("/")
    public String home() {
        log.info("Handling home");
        return "Value : "+myConfig.getAdvanceBookingDays();
    }

}
