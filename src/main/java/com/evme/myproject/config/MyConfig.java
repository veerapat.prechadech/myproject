package com.evme.myproject.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class MyConfig {

    @Value("${advance.booking.days}")
    private String advanceBookingDays;

}
